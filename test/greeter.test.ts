import { Greeter } from '../src/greeter.js'

describe('Greeter', () => {
  describe('Given the greeter has no name', () => {
    const greeter = new Greeter()

    test('When greeting, Then the name used is "Mysterious Bob"', () => {
      expect(greeter.greet()).toBe('Greetings, my name is Mysterious Bob!')
    })
  })

  describe('Given the greeter has name "Sneaky Pete"', () => {
    const greeter = new Greeter('Sneaky Pete')

    test('When greeting, Then the name used is "Sneaky Pete"', () => {
      expect(greeter.greet()).toBe('Greetings, my name is Sneaky Pete!')
    })
  })
})
