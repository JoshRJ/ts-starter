export class Greeter {
  private _name: string = 'Mysterious Bob'

  constructor(name?: string) {
    if (name) {
      this._name = name
    }
  }

  public greet(): string {
    return `Greetings, my name is ${this._name}!`
  }
}
